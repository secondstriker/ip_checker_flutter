import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';

void main() {
  runApp(MaterialApp(builder: (context, child) => const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _ipAddress1 = "Loading ...";
  String _ipAddress2 = "Loading ...";
  String _ipAddress3 = "Loading ...";
  String _ipAddress4 = "Loading ...";
  String _ipAddress5 = "Loading ...";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _future(),
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) =>
            Scaffold(
              backgroundColor: Colors.white,
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(_ipAddress1, textAlign: TextAlign.center,),
                  Text(_ipAddress2, textAlign: TextAlign.center,),
                  Text(_ipAddress3, textAlign: TextAlign.center,),
                  Text(_ipAddress4, textAlign: TextAlign.center,),
                  Text(_ipAddress5, textAlign: TextAlign.center,),
                ],
              ),
            ));
  }

  Future _future() async {
    await printIps();
  }

  Future printIps() async {
    WidgetsFlutterBinding.ensureInitialized();
    Future.delayed(const Duration(seconds: 5), () async {

     final ipAddressList = await Future.wait([
        _setIp1(),
        _setIp2(),
        _setIp3(),
        _setIp4(),
        _setIp5(),
      ]);
     setState(() {
       _ipAddress1 = ipAddressList[0];
       _ipAddress2 = ipAddressList[1];
       _ipAddress3 = ipAddressList[2];
       _ipAddress4 = ipAddressList[3];
       _ipAddress5 = ipAddressList[4];
     });
    });
  }

  int count1 = 0;
  Future<String> _setIp1() async {
    final ipAddress = "1 => trying #${++count1}, ip: ${await Ipify.ipv4()}";
    debugPrint(ipAddress);
    return ipAddress;
  }

  int count2 = 0;
  Future<String> _setIp2() async {
    final ipAddress = "2 => trying #${++count2}, ip: ${await Ipify.ipv4()}";
    debugPrint(ipAddress);
    return ipAddress;
  }

  int count3 = 0;
  Future<String> _setIp3() async {
    final ipAddress = "3 => trying #${++count3}, ip: ${await Ipify.ipv4()}";
    debugPrint(ipAddress);
    return ipAddress;
  }

  int count4 = 0;
  Future<String> _setIp4() async {
    final ipAddress = "4 => trying #${++count4}, ip: ${await Ipify.ipv4()}";
    debugPrint(ipAddress);
    return ipAddress;
  }

  int count5 = 0;
  Future<String> _setIp5() async {
    final ipAddress = "5 => trying #${++count5}, ip: ${await Ipify.ipv4()}";
    debugPrint(ipAddress);
    return ipAddress;
  }
}

enum Format { TEXT, JSON, JSONP }

class Ipify extends NetworkManager with NativeDioNetworkManager {
  static const String _scheme = 'https';
  static const String _hostv4 = 'api.ipify.org';
  static const String _hostv64 = 'api64.ipify.org';
  static const String _path = '/';
  static const String _format = 'format=';

  Ipify._();

  static final Ipify _instance = Ipify._();

  static Future<String> ipv4({Format format = Format.TEXT}) async {
    final uri =
        Uri(host: _hostv4, path: _path, scheme: _scheme, query: _param(format));
    return await _instance._get(uri);
  }

  static Future<String> ipv64({Format format = Format.TEXT}) async {
    final uri = Uri(
        host: _hostv64, path: _path, scheme: _scheme, query: _param(format));
    return await _instance._get(uri);
  }

  static String _param(Format format) =>
      _format +
      (Format.TEXT == format
          ? ''
          : Format.JSON == format
              ? 'json'
              : 'jsonp');
}

abstract interface class NetworkManager {
  Future<String> _get(Uri uri);
}

mixin DioNetworkManager on NetworkManager {
  late final _dio = _getDio();

  Dio _getDio() {
    final dio = Dio(BaseOptions(
        baseUrl: "https://api.ipify.org",
        connectTimeout: const Duration(seconds: 5),
        receiveTimeout: const Duration(seconds: 3),
 //       persistentConnection: true,
    ));
    return dio;
  }

  @override
  Future<String> _get(Uri uri) async {
    try {
      final response = await Dio().getUri(uri);

      if (response.statusCode != 200) {
        return "Could not get IP, retrying...";
      }

      return response.data;
    } catch (e) {
      return "Could not get IP, retrying...";
    }
  }
}

mixin NativeDioNetworkManager on NetworkManager {
  late final _dio = _getDio();

  Dio _getDio() {
    final dio = DioForNative(BaseOptions(
        baseUrl: "https://api.ipify.org",
        connectTimeout: const Duration(seconds: 5),
        receiveTimeout: const Duration(seconds: 3),
    ));
    dio.httpClientAdapter = IOHttpClientAdapter(createHttpClient: () {
      return HttpClient()..maxConnectionsPerHost = 1
      ..idleTimeout = const Duration(seconds: 20);
    });
    return dio;
  }

  @override
  Future<String> _get(Uri uri) async {
    try {
      final response = await _dio.getUri(uri);

      if (response.statusCode != 200) {
        return "Could not get IP, retrying...";
      }

      return response.data;
    } catch (e) {
      return "Could not get IP, retrying...";
    }
  }
}

mixin HttpNetworkManager on NetworkManager {
  @override
  Future<String> _get(Uri uri) async {
    try {
      final response = await http.get(uri);

      if (response.statusCode != 200) {
        return "Could not get IP, retrying...";
      }

      return response.body;
    } catch (e) {
      return "Could not get IP, retrying...";
    }
  }
}

mixin IOHttpNetworkManager on NetworkManager {
  @override
  Future<String> _get(Uri uri) async {
    try {
      final response = await IOClient().get(uri);

      if (response.statusCode != 200) {
        return "Could not get IP, retrying...";
      }

      return response.body;
    } catch (e) {
      return "Could not get IP, retrying...";
    }
  }
}
